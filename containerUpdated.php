<html lang="en">
<head>
    <?php
    include 'includes.php';
    ?>
    <title>Container Bought</title>

</head>

<body>

<link rel="stylesheet" type="text/css" href="myStyle.css">

<div class="container">
<?php

if(isset($_POST)){
    $everything = [
        'first_name',
        'last_name',
        'email',
    ];

    $missing = [];

    foreach($everything as $one) {
        if(empty($_POST[$one])) {
            $missing[] = $one;
        }
    }
}


if(empty($missing)) {

    include_once 'mysqli_connect.php';

    $_SESSION['uname'] = $_POST['first_name'];

    $query = "UPDATE`mydb`.`users` SET `userName`='".$_POST['first_name']."', `userLastname`='".$_POST['last_name']."', `email`='".$_POST['email']."'WHERE user_id = ".$_POST['user_id'];

    mysqli_query($dbc, $query);

    $query = "UPDATE`mydb`.`sales` SET `container_id`='".$_POST['containerRadio']."'WHERE user_id = ".$_POST['user_id'];
    mysqli_query($dbc, $query);

    $id = $_POST['user_id'];

    $query = "SELECT s.datetime,
    u.userName,
    u.userLastname,
    u.email,
    c.container_name,
    c.width,
    c.height,
    c.length,
    c.price FROM sales s
        JOIN
    users u ON s.user_id = u.user_id
        JOIN
    container c ON s.container_id = c.container_id
    WHERE s.user_id = ".$id."
      ORDER BY s.datetime DESC";

    $result = mysqli_query($dbc, $query);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    echo "<h2>Your last purchase is</h2>";

    echo "<table class='table'  align=\"left\" cellspacing=\"5\" cellpadding=\"8\">";
    echo '<tr>';
    foreach(array_keys($row) as $column) {
        echo '<td>';
        echo $column;
        echo '</td>';
    }
    echo '</tr>';

    echo '<tr>';
    foreach(array_values($row) as $value) {
        echo '<td>';
        echo $value;
        echo '</td>';
    }
    echo '</tr>';
    echo "</table>";
}
else {

    echo 'You need to enter the following data<br />';

    foreach ($missing as $one) {

        echo $one."<br />";

    }
}


?>
<br/>
<br/>
<br/>

<form action='updateContainer.php' method='post' name='frm'>
    <?php
    foreach ($_POST as $a => $b) {
        echo "<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>";
    }
    ?>
    <input type="submit" class="btn btn-default" value="Edit purchase">
</form>


    <button class="btn btn-danger" onclick="location.href='deletePurchase.php?id=<?= $id ?>'">Delete Purchase</button>
    <button  class="myButton margin-20px "  onclick="location.href='index.php'">Home</button>
</div>
</body>
</html>