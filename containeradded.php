<html>

<head>

    <title>Container Bought</title>

</head>
<body>

<link rel="stylesheet" type="text/css" href="myStyle.css">

<?php

if(isset($_POST)){
    $everything = [
        'first_name',
        'last_name',
        'email',
    ];

    $missing = [];

    foreach($everything as $one) {
        if(empty($_POST[$one])) {
            $missing[] = $one;
        }
    }
}


    if(empty($missing)) {

        include_once 'mysqli_connect.php';

        $query = "INSERT INTO `mydb`.`users` (`userName`, `userLastname`, `email`) VALUES ('".$_POST['first_name']."', '".$_POST['last_name']."', '".$_POST['email']."')";

        mysqli_query($dbc, $query);
        $id = mysqli_insert_id($dbc);

        $query = "INSERT INTO `mydb`.`sales` (`user_id`, `container_id`,`datetime`) VALUES ('".$id."', '".$_POST['containerRadio']."', NOW())";
        mysqli_query($dbc, $query);


        $query = "SELECT s.datetime,
    u.userName,
    u.userLastname,
    u.email,
    c.container_name,
    c.width,
    c.height,
    c.length,
    c.price FROM sales s
        JOIN
    users u ON s.user_id = u.user_id
        JOIN
    container c ON s.container_id = c.container_id
    WHERE s.user_id = ".$id."
      ORDER BY s.datetime DESC";

        $result = mysqli_query($dbc, $query);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        echo "Your last purchase is</br>";

        echo "<table  align=\"left\" cellspacing=\"5\" cellpadding=\"8\">";
        echo '<tr>';
        foreach(array_keys($row) as $column) {
            echo '<td>';
            echo $column;
            echo '</td>';
        }
        echo '</tr>';

        echo '<tr>';
        foreach(array_values($row) as $value) {
            echo '<td>';
            echo $value;
            echo '</td>';
        }
        echo '</tr>';
        echo "</table>";
    }
    else {

        echo 'You need to enter the following data<br />';

        foreach ($missing as $one) {

            echo $one."<br />";

        }
    }



?>
<br/>
<br/>
<br/>


<form action='updateContainer.php' method='post' name='frm'>
    <?php
    foreach ($_POST as $a => $b) {
        echo "<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>";
    }

    echo "<input type='hidden' name='user_id' value='".$id."'>";
    ?>
    <input type="submit" value="Edit purchase">
</form>
<button class="btn btn-danger myButton" onclick="location.href='deletePurchase.php?id=<?= $id ?>'">Delete Purchase</button>

</body>

</html>