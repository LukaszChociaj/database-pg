<html>
<head>
    <h1>All purchases</h1>
</head>
<body>
<link rel="stylesheet" type="text/css" href="myStyle.css">
<?php
// Get a connection for the database
include_once 'mysqli_connect.php';

// Create a query for the database
$query = "SELECT s.datetime,
    u.userName,
    u.userLastname,
    u.email,
    c.container_name,
    c.width,
    c.height,
    c.length,
    c.price FROM sales s
        JOIN
    users u ON s.user_id = u.user_id
        JOIN
    container c ON s.container_id = c.container_id
      ORDER BY s.datetime DESC";

// Get a response from the database by sending the connection
// and the query
$response = @mysqli_query($dbc, $query);

// If the query executed properly proceed
$row = mysqli_fetch_array($response, MYSQLI_ASSOC);


if($response){

    echo '<table align="left" cellspacing="5" cellpadding="8"  style="width:100%">';

    echo '<tr>
        <td align="left"><b>Date</b></td>
        <td align="left"><b>First Name</b></td>
        <td align="left"><b>Last Name</b></td>
        <td align="left"><b>Email</b></td>
        <td align="left"><b>Container type</b></td>
        <td align="left"><b>Width</b></td>
        <td align="left"><b>Heigth</b></td>
        <td align="left"><b>Length</b></td>
        <td align="left"><b>Price</b></td>
    </tr>';

    // mysqli_fetch_array will return a row of data from the query
    // until no further data is available
    while($row = mysqli_fetch_array($response)){
        echo'<tr>'.
            '<td align="left">' .$row['datetime'] . '</td>'.
            '<td align="left">' .$row['userName'] . '</td>'.
            '<td align="left">' .$row['userLastname'] . '</td>'.
            '<td align="left">' .$row['email'] . '</td>'.
            '<td align="left">' .$row['container_name'] . '</td>'.
            '<td align="left">' .$row['width'] . '</td>'.
            '<td align="left">' .$row['height'] . '</td>'.
            '<td align="left">' .$row['length'] . '</td>'.
            '<td align="left">' .$row['price'] . '</td>'.
            '</tr>';
    }
    echo '</table>';

} else {
    echo "Couldn't issue database query<br />";
    echo mysqli_error($dbc);
}

// Close connection to the database
mysqli_close($dbc);
?>


<div>
    <button  class="myButton margin-20px "  onclick="location.href='index.php'">Home</button>
</div>


</body>
</html>