<html>
<head>
    <title>Buy a container</title>
</head>
<body>

<link rel="stylesheet" type="text/css" href="myStyle.css">

<?php
// Get a connection for the database
include_once 'mysqli_connect.php';

// Create a query for the database

$query = "SELECT * FROM container";

// Get a response from the database by sending the connection
// and the query

$response = @mysqli_query($dbc, $query);

if($response){
    echo '<table align="left" cellspacing="5" cellpadding="8" style="width:100%">';

    echo '<tr>
        <td align="left"><b>Container Name</b></td>
        <td align="left"><b>Width</b></td>
        <td align="left"><b>Height</b></td>
        <td align="left"><b>Length</b></td>
        <td align="left"><b>Price</b></td>
    </tr>';

    // mysqli_fetch_array will return a row of data from the query
    // until no further data is available
    while($row = mysqli_fetch_array($response)){
        echo'<tr>'.
            '<td align="left">' .$row['container_name'] . '</td>'.
            '<td align="left">' .$row['width'] . '</td>'.
            '<td align="left">' .$row['height'] . '</td>'.
            '<td align="left">' .$row['length'] . '</td>'.
            '<td align="left">' .$row['price'] . '</td>'.
            '</tr>';

    }

    echo '</table>';

} else {
    echo "Couldn't issue database query<br />";
    echo mysqli_error($dbc);
}
?>

<div>
    <b>Put Your details below</b>

    <form method="post" action="containeradded.php">

        <label>First Name:<br>
            <input type="text" name="first_name" size="30" value="" />
        </label>
        <br>
        <label>Last Name:<br>
            <input type="text" name="last_name" size="30" value="" />
        </label>
        <br>
        <label>Email:<br>
            <input type="text" name="email" size="30" value="" />
        </label>
        <br>
        <label>
            Container Type<br>
        </label>

        <label>
            <?php
            mysqli_data_seek($response, 0);
            $i=0;
            while($row = mysqli_fetch_array($response)){
                echo '<td><input type = "radio" name ="containerRadio" value = "'.$row['container_id'].'" '.($i++ == 0? 'checked' : '') .'>'.$row['container_name'].'</td><br>';
            }
            ?>
        </label>

        <input type="submit" name="submit" value="Send" />
    </form>

</div>
</body>
</html>
